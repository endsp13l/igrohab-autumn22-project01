using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 45f;
    public int healthBonus = 20;
    
    void Update()
    {
        transform.Rotate(Vector3.up, rotationSpeed * Time.fixedDeltaTime);
    }
}
