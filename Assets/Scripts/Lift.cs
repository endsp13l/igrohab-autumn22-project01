using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour
{
    public Game LiftControl;
    public Vector3 positionChange = Vector3.up;
    public bool Launch = false;
    public void Update()
    {
        Launch = LiftControl.LiftLaunch;
        
        if (Launch && transform.position.y <0)
            transform.position += positionChange * Time.deltaTime;
    }

}
